//Take 7 characters as input. Print only vowels from array.



import java.io.*;
class P4{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int n=Integer.parseInt(br.readLine());

		char arr[]=new char[n];

		System.out.println("Enter elements");
	
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)br.read();
			br.skip(1);

		}

		for(int i=0;i<arr.length;i++){

			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u'){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();

		
	}
}



