//Print product of even elements only.


import java.io.*;

class P2{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int n=Integer.parseInt(br.readLine());

		int arr[]=new int[n];

		System.out.println("Enter elements");
		int prod=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				prod=prod*arr[i];
			}
		}

		System.out.println("Product of even elements is "+prod);
	}
}



